SELECT 
  s.store_id,
  s.manager_staff_id,
  CONCAT(st.first_name, ' ', st.last_name) AS staff_name,
  SUM(p.amount) AS total_revenue
FROM
  store s
  JOIN staff st ON s.store_id = st.store_id
  JOIN payment p ON st.staff_id = p.staff_id
  JOIN rental r ON p.rental_id = r.rental_id
WHERE
  EXTRACT(YEAR FROM r.rental_date) = 2017
GROUP BY
  s.store_id,
  s.manager_staff_id,
  st.staff_id,
  st.first_name,
  st.last_name
HAVING
  SUM(p.amount) = (
    SELECT MAX(total_revenue)
    FROM (
      SELECT
        s.store_id,
        SUM(p.amount) AS total_revenue
      FROM
        store s
        JOIN staff st ON s.store_id = st.store_id
        JOIN payment p ON st.staff_id = p.staff_id
        JOIN rental r ON p.rental_id = r.rental_id
      WHERE
        EXTRACT(YEAR FROM r.rental_date) = 2017
      GROUP BY
        s.store_id
    ) subquery
    WHERE subquery.store_id = s.store_id
  );