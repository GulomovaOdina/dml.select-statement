WITH actor_films AS (
    SELECT actor_id, MIN(f.release_year) AS min_year, MAX(f.release_year) AS max_year
    FROM film_actor fa
    INNER JOIN film f ON fa.film_id = f.film_id
    GROUP BY actor_id
)
SELECT a.actor_id, CONCAT(a.first_name, ' ', a.last_name) AS actor_name
FROM actor a
LEFT JOIN actor_films af ON a.actor_id = af.actor_id
WHERE NOT EXISTS (
    SELECT 1
    FROM actor_films af2
    WHERE af2.min_year < af.min_year OR (af2.min_year = af.min_year AND af2.max_year > af.max_year)
);